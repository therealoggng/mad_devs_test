Django==3.2
django-environ==0.4.5
djangorestframework==3.12.4
djangorestframework-simplejwt==4.4.0
PyJWT==1.7.1
psycopg2-binary==2.8.4
django-filter==2.4.0
