FROM python:3.8.7

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV TZ=Asia/Bishkek

RUN python3 -m pip install --upgrade pip

RUN mkdir /source
WORKDIR /source
COPY requirements.txt /source/
RUN pip3 install -r requirements.txt

COPY . /source/
CMD python manage.py migrate & python manage.py loaddata dbdump.json & python manage.py runserver 0.0.0.0:8000
