from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from items.views import DiagnosesViewSet, PatientViewSet, RegisterView, MyTokenObtainPairView

# I'm using a router here because it makes the code shorter and works great with viewsets

router = DefaultRouter()
router.register(r'diagnoses', DiagnosesViewSet, basename='diagnoses')
router.register(r'patient', PatientViewSet, basename='patient')


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include(router.urls)),
    path('api/v1/token/', MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/v1/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/v1/register/', RegisterView.as_view(), name='register'),

]
