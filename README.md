# REST API for Hospital

This project was developed in order to demonstrate its qualities.
The project includes two models of Diagnoses and Patients. The application includes user registration, the API is available only to those who have the label is Doctor == True, this field was added to the user with the extension abstract user. Authorization via the JWT token to get it the instructions will be below

##Requirements
Dependencies such as 

    Django==3.2
    django-environ==0.4.5
    djangorestframework==3.12.4
    djangorestframework-simplejwt==4.4.0
    PyJWT==1.7.1
    psycopg2-binary==2.8.4
    django-filter==2.4.0

## Install
In my case I use https cloning

    git clone https://gitlab.com/therealoggng/mad_devs_test.git


## Run the app
First we need to make sure that we have all the dependencies installed on the software and are in working order related to the Docker.
```Also, when docker starts , makes migrations and puts a fixture in the database , you don 't have to do everything manually. ```

    docker-compose up -d --build 

`or`

    sudo docker-compose up -d --build 


## Run the tests

    docker-compose exec hospital python manage.py test

# REST API

The REST API to the example app is described below.

## Register
Well, let's get started. At the bottom you have to register in our system

### Request

`POST /api/v1/register/`

    http://localhost:8000/api/v1/register/

Body requests:
    
    {
    "username": "your_username",
    "password": "your_password",
    "password2": "your_password2",
    "email": "your_email@gmail.com",
    "first_name": "Your_first_name",
    "last_name": "Your_last_name"
}

### Response

    HTTP 201 Created
    Allow: POST, OPTIONS
    Content-Type: application/json
    Vary: Accept
    
    {
        "username": "your_username",
        "email": "your_email@gmail.com",
        "first_name": "Your_first_name",
        "last_name": "Your_last_name"
    }

## GET access isDoctor

A token without confirmation that you are a doctor will not give access to endpoints, go to the admin panel and give access to your account a label that you are a doctor
    
    http://localhost:8000/admin/

    [username: admin  
     password: admin ]

![img.png](img.png)

## Get Token 

### Request

`POST /api/v1/token/`

    http://localhost:8000/api/v1/token/

Body requests:

    'username=your_username&password=your_password'

### Response

    HTTP 200 OK
    Allow: POST, OPTIONS
    Content-Type: application/json
    Vary: Accept
    
    {
        "refresh": "xxxxxxxxxxxxxxxxxxxxxxxxJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTYzNjA5ODE2NSwianRpIjoiNjI2OWY1MWFlYzc3NDQyY2I0ODQzYjk5NDE3MTk2OTYiLCJ1c2VyX2lkIjozLCJ1c2VybmFtZSI6ImJvYiIsImlzRG9jdG9yIjp0cnVlfQ._oLLNkI80Pf8tRaue_OhXGVQNI4OFHahpvmeYibqUdw",
        "access": "xxxxxxxxxxxxxxxxxxxxxxxxiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjM2MDE1MzY1LCJqdGkiOiI1N2QzMzhhNmE3ZmI0NDA4OWYzMmViNzU5Nzg1NTg1YiIsInVzZXJfaWQiOjMsInVzZXJuYW1lIjoiYm9iIiwiaXNEb2N0b3IiOnRydWV9.M1S8TquTSxCVzu23eZqmKTakKpfYbZLr3skhujTPlOA"
    }


## Get list of Diagnoses

### Request

`GET /api/v1/diagnoses/`

    curl -i -H "Authorization: Bearer <ACCESS_TOKEN>" http://localhost:8000/api/v1/diagnoses/

### Response

    HTTP 200 OK
    Allow: GET, POST, HEAD, OPTIONS
    Content-Type: application/json
    Vary: Accept
    
    [
        {
            "id": 1,
            "name": "СЕРДЕЧНОСОСУДИСТЫЕ ЗАБОЛЕВАНИЯ"
        },
        {
            "id": 2,
            "name": "ДИАБЕТ"
        },
        {
            "id": 3,
            "name": "ТУБЕРКУЛЕЗ"
        },
        .......
    ]

## Get list of Patients

### Request

`GET /api/v1/patient/`

    curl -i -H "Authorization: Bearer <ACCESS_TOKEN>" http://localhost:8000/api/v1/patient/

### Response

    HTTP 200 OK
    Allow: GET, POST, HEAD, OPTIONS
    Content-Type: application/json
    Vary: Accept
    
    [
        {
            "id": 1,
            "date_of_birth": "1991-01-03",
            "diagnoses": [
                "ТУБЕРКУЛЕЗ",
                "ДИАРЕЯ",
                "СПИД"
            ],
            "created_at": "2021-11-03T21:44:05.269295+06:00"
        },
        {
            "id": 3,
            "date_of_birth": "1987-04-11",
            "diagnoses": [
                "СЕРДЕЧНОСОСУДИСТЫЕ ЗАБОЛЕВАНИЯ",
                "МАЛЯРИЯ",
                "ОБСТРУКТИВНАЯ БОЛЕЗНЬ ЛЕГКИХ"
            ],
            "created_at": "2021-11-03T22:20:11.557144+06:00"
        },
        {
            "id": 4,
            "date_of_birth": "1969-12-10",
            "diagnoses": [
                "ТУБЕРКУЛЕЗ",
                "СПИД",
                "ЦЕРЕБРОВАСКУЛЯРНЫЕ ЗАБОЛЕВАНИЯ"
            ],
            "created_at": "2021-11-03T22:22:05.171004+06:00"
        }
    ]


## Authors

~ Shirinbekov Kutman  - [GitHub](https://github.com/therealoggng)

My email address (simpleshirinbekov@gmail.com)

### ALL ENJOY PROGRAMMING