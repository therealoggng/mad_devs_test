from rest_framework import viewsets, filters, generics
from rest_framework.permissions import AllowAny, IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend
from items.models import CustomUser, Diagnoses, Patient
from items.serializers import (DiagnosesSerializer, PatientSerializer, PatientListSerializer,
                               RegisterSerializer, MyTokenObtainPairSerializer)
from items.permissions import UserIsDoctor
from rest_framework_simplejwt.views import TokenObtainPairView


# view for custom token
class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


# Create a RegisterView with a post action
# CreateAPIView used for create-only endpoints
class RegisterView(generics.CreateAPIView):
    queryset = CustomUser.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer


#  mostly worked here with ViewSets which
#  includes all methods(GET,LIST,DETAIL,POST,UPDATE,DELETE)
class DiagnosesViewSet(viewsets.ModelViewSet):
    queryset = Diagnoses.objects.all()
    permission_classes = (UserIsDoctor,)
    serializer_class = DiagnosesSerializer
    filter_backends = [filters.SearchFilter] # <- here I installed a search
    search_fields = ['name']                 # engine just if there is a lot of data


class PatientViewSet(viewsets.ModelViewSet):
    queryset = Patient.objects.all()
    permission_classes = (UserIsDoctor,)
    serializer_class = PatientSerializer
    filter_backends = [DjangoFilterBackend]           # <- and here I made a filter to pull
    filterset_fields = ['date_of_birth', 'created_at'] # out patients by date of birth or by creating a record

    def get_serializer_class(self, *args, **kwargs):
        if self.action == 'list':
            return PatientListSerializer
        return PatientSerializer
