from rest_framework import permissions


class UserIsDoctor(permissions.BasePermission):
    #here is implemented its own access check, it is checked by the doctor or not,
    #otherwise he will be denied access
    def has_permission(self, request, view):
        try:
            if request.user.isDoctor == True:
                return True
        except AttributeError:
            pass
        return False
