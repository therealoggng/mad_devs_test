from rest_framework import serializers
from items.models import CustomUser, Diagnoses, Patient
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

# we added an additional field so that it is generated in the token
class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        token['username'] = user.username
        token['isDoctor'] = user.isDoctor
        return token


class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=CustomUser.objects.all())]
    )

    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = CustomUser    ## These are the fields that our registration form is contains.
        fields = ('username', 'password', 'password2', 'email', 'first_name', 'last_name')
        extra_kwargs = {
            'first_name': {'required': True}, # <- We can add extra validations with extra_kwargs option. We set first_name and last_name required.
            'last_name': {'required': True}
        }

    #Password fields must be same. We can validate these
       # fields with serializers validate(self, attrs) method:
    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})

        return attrs

    #When send POST request to register endpoint,
        # it calls RegisterSerializer’s create method which saves user object
    def create(self, validated_data):
        user = CustomUser.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )

        user.set_password(validated_data['password'])
        user.save()

        return user


#make serializers for Diagnoses
class DiagnosesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Diagnoses
        fields = ('id', 'name')


# make serializers for Patient, also make nested serializers on diagnoses
class PatientListSerializer(serializers.ModelSerializer):
    diagnoses = serializers.StringRelatedField(many=True)

    class Meta:
        model = Patient
        fields = ('id', 'date_of_birth', 'diagnoses', 'created_at')


class PatientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Patient
        fields = ('id', 'date_of_birth', 'diagnoses', 'created_at')