from django.test import TestCase
from rest_framework.reverse import reverse
from items.models import Diagnoses, Patient
from datetime import date
from django.contrib.auth import get_user_model


class DiagnosesTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        Diagnoses.objects.create(name='Рак желудка')

    def test_string_method(self):
        diagnoses = Diagnoses.objects.get(id=1)
        expected_string = diagnoses.name
        self.assertEqual(str(diagnoses), expected_string)

    def test_name_max_length_success(self):
        diagnoses = Diagnoses.objects.get(id=1)
        max_length = diagnoses._meta.get_field('name').max_length
        self.assertEquals(max_length, 155)

    def test_name_max_length_failed(self):
        diagnoses = Diagnoses.objects.get(id=1)
        max_length = diagnoses._meta.get_field('name').max_length
        self.assertNotEquals(max_length, 255)


class PatientTestCase(TestCase):
    def setUp(self):
        self.user = self.setup_user()
        User = get_user_model()
        User.objects.create_user(
            username='test_user_1',
            email='test_user_1@mail.com',
            password='test_user_1'
        )
        User.objects.create_user(
            username='test_user_2',
            email='test_user_2@mail.com',
            password='test_user_2'
        )


    @staticmethod
    def setup_user():
        CustomUser = get_user_model()
        return CustomUser.objects.create_user(
            username='admin',
            email='admin@test.com',
            password='admin',
            isDoctor=True
        )


    def get_token(self):
        url = reverse('token_obtain_pair')
        data = {"username": "admin", "password": "admin"}
        response = self.client.post(url, data, format='json')
        return response.data

    @classmethod
    def setUpTestData(cls):
        d1 = Diagnoses.objects.create(name='Рак желудка')
        d2 = Diagnoses.objects.create(name='Опухоль мозга')
        d3 = Diagnoses.objects.create(name='Мигрень')
        d4 = Diagnoses.objects.create(name='Анемия')
        d5 = Diagnoses.objects.create(name='Гастрит')
        d6 = Diagnoses.objects.create(name='Гепатит')
        patient1 = Patient.objects.create(date_of_birth=date(1991, 7, 29))
        patient2 = Patient.objects.create(date_of_birth=date(1994, 9, 30))
        patient1.diagnoses.set([d1, d4, d6])
        patient2.diagnoses.set([d2, d3, d5])

    # def test_create_patient(self):
    #     url = reverse('patient-list')
    #     token = str(self.get_token())
    #     print(token)
    #     data = {'date_of_birth': date(1990, 7, 29),
    #             'diagnoses': [1, 2, 3]}
    #     client = APIClient()
    #     client.credentials(HTTP_AUTHORIZATION='Bearer' + token)
    #     response = self.client.post(url, data, format='json')
    #     print(response.data)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(Patient.objects.get(id=3).date_of_birth, date(1990, 7, 29))
    #     self.assertEqual(Patient.objects.get(id=3).sort.get(id=1).name, 'Рак желудка')
