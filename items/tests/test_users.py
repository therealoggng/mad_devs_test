from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
import jwt


class SignUpTest(APITestCase):

    def test_register(self):
        url = reverse('register')
        CustomUser = get_user_model()
        data = {"username": "test", "first_name": "Alan", "last_name": "Pardew", "email": "test@test.com",
                "password": "quickstart12345678", "password2": "quickstart12345678"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CustomUser.objects.get(id=1).username, 'test')


    def test_register_mismatch_password(self):
        url = reverse('register')
        data = {"username": "test", "first_name": "Alan", "last_name": "Pardew", "email": "test@test.com",
                "password": "quickstart12345678", "password2": "test"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_register_on_weak_password(self):
        url = reverse('register')
        data = {"username": "test", "first_name": "Alan", "last_name": "Pardew", "email": "test@test.com",
                "password": "test", "password2": "test"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_register_on_common_password(self):
        url = reverse('register')
        data = {"username": "test", "first_name": "Alan", "last_name": "Pardew", "email": "test@test.com",
                "password": "qwerty123", "password2": "qwerty123"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TokenTest(APITestCase):
    def setUp(self):
        self.user = self.setup_user()
        User = get_user_model()
        User.objects.create_user(
            username='test_user_1',
            email='test_user_1@mail.com',
            password='test_user_1'
        )
        User.objects.create_user(
            username='test_user_2',
            email='test_user_2@mail.com',
            password='test_user_2'
        )


    @staticmethod
    def setup_user():
        CustomUser = get_user_model()
        return CustomUser.objects.create_user(
            username='admin',
            email='admin@test.com',
            password='admin',
            isDoctor=True
        )


    def get_token(self):
        url = reverse('token_obtain_pair')
        data = {"username": "admin", "password": "admin"}
        response = self.client.post(url, data, format='json')
        decode = jwt.decode(response.data['access'], verify=False)
        return decode

    def test_check_success_token(self):
        url = reverse('token_obtain_pair')
        data = {"username": "admin", "password": "admin"}
        response = self.client.post(url, data, format='json')
        decode = jwt.decode(response.data['access'],verify=False)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(decode['username'], data['username'])
        self.assertTrue(response.data)

    def test_check_is_Doctor(self):
        url = reverse('token_obtain_pair')
        data = {"username": "admin", "password": "admin"}
        response = self.client.post(url, data, format='json')
        decode = jwt.decode(response.data['access'], verify=False)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(decode['isDoctor'], self.user.isDoctor)
        self.assertTrue(response.data)