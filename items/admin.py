from django.contrib import admin
from items.models import CustomUser


class UserAdmin(admin.ModelAdmin):
    fields = ['first_name', 'last_name', 'isDoctor']
    list_display = ['first_name', 'last_name', 'isDoctor']


admin.site.register(CustomUser, UserAdmin)
