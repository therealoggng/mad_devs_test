from django.db import models
from django.contrib.auth.models import AbstractUser,  UserManager as AbstractUserManager


# create manager for user, to expand and not touch others
class UserManager(AbstractUserManager):
  pass


# here we expand by adding a field and return the merged first and last name
class CustomUser(AbstractUser):
    isDoctor = models.BooleanField(default=False)
    objects = UserManager()

    def __str__(self):
        return self.first_name + ' ' + self.last_name

# this create diagnoses, fields(id, name)
class Diagnoses(models.Model):
    name = models.CharField(max_length=155, unique=True)

    def __str__(self):
        return self.name

# model patient created obj by fields(id, date_of_birth,
                             # diagnoses(fk), created_at)
class Patient(models.Model):
    date_of_birth = models.DateField(blank=True, null=True)
    diagnoses = models.ManyToManyField(Diagnoses, related_name='patient_diagnoses')
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.created_at
